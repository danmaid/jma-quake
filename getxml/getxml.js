const EventSource = require('eventsource')
const Rx = require('rxjs/Rx')
const fetch = require('node-fetch')
const xml2js = require('xml2js')
const express = require('express')
const events = require('events')

var app = express()
var broker = new events.EventEmitter()

setInterval(function() {
    broker.emit('keepalive')
}, 30000)

app.get('/', function (req,res) {
  res.sendFile(__dirname+ '/public/index.html')
})

var quakeSource = new Rx.Subject()
var source6912 = new EventSource('http://localhost:6912/follow')
source6912.onmessage = function(e) {
    console.log('event received: ', e)
    // JSONを展開してストリームに流す
    quakeSource.next(JSON.parse(e.data))
}
quakeSource
.filter(x => x.link) // link があるもののみ
.subscribe(next => {
    console.log('next: ', next.link)
    fetch(next.link)
    .then(x => {
        console.log(x)
        return x.text()
    })
    .then(xml => {
        console.log(xml)
        xml2js.parseString(xml, (err, result) => {
            // console.log(JSON.stringify(result))
            broker.emit('quake', result)
        })
    })
})
source6912.onerror = function(e) {
    console.log(e)
}

app.get('/follow', function (req,res) {
    console.log('connected')
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    })
    res.write('\n')

    var f = function(d) {
        res.write('data: ' + JSON.stringify(d) + '\n\n')
        res.flushHeaders()
    }
    broker.on('quake', f)
    var k = function(d) {
        res.write(': keepalive\n\n')
        res.flushHeaders()
    }
    broker.on('keepalive', k)

    req.on('close', function() {
        broker.removeListener('quake', f)
        console.log('closed')
    })
})

app.listen(6913, function () {
   console.log('getxml start at port: 6913')
})
