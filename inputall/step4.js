const express = require('express')
const events = require('events')
const EventSource = require('eventsource')
const Rx = require('rxjs/Rx')
const fetch = require('node-fetch')
const xml2js = require('xml2js')

const app = express()
const broker = new events.EventEmitter()

setInterval(function() {
    broker.emit('keepalive')
}, 30000)

var destination = new Rx.Subject()
// var source = new EventSource('http://localhost:6913/follow')
var source = new EventSource('http://localhost:6912/follow')
source.onmessage = function(e) {
    console.log('source event received: ', e)
    // JSONを展開してストリームに流す
    destination.next(JSON.parse(e.data))
}
source.onerror = function(e) {
    console.log('source error: ', e)
}
source.onopen = function(e) {
    console.log('source open: ', e)
}

destination
.filter(x => x.link[0].$.href)
.map(x => x.link[0].$.href)
.subscribe(next => {
    console.log('next: ', next)
    fetch(next)
    .then(x => {
        console.log(x)
        return x.text()
    })
    .then(xml => {
        console.log(xml)
        xml2js.parseString(xml, (err, result) => {
            // console.log(JSON.stringify(result))
            broker.emit('event', result)
        })
    })
})

app.use(express.static(__dirname+ '/public'))

app.get('/status', function(req, res) {
    let status = {
        'readyState': source.readyState,
        'url': source.url,
        'clientCount': broker.listenerCount('event')
    }
    console.log(broker.listeners('event'))
    res.header('Content-Type', 'application/json; charset=utf-8')
    res.send(JSON.stringify(status))
})

app.get('/follow', function (req,res) {
    console.log('connected')
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Access-Control-Allow-Origin': '*'
    })
    res.write('\n')

    var f = function(d) {
        res.write('data: ' + JSON.stringify(d) + '\n\n')
        res.flushHeaders()
    }
    broker.on('event', f)

    var k = function(d) {
        res.write(': keepalive\n\n')
        res.flushHeaders()
    }
    broker.on('keepalive', k)

    req.on('close', function() {
        broker.removeListener('event', f)
        broker.removeListener('keepalive', k)
        console.log('closed')
    })
})

app.listen(6914, function () {
   console.log('fetcher start at port: 6914')
})
