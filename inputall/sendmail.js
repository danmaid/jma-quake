const EventSource = require('eventsource')
const mailer = require('nodemailer');
const Rx = require('rxjs/Rx')

const mailSettings = {
    host: 'smtp-relay.gmail.com',
    port: '587'
}

var mailOptions = {
    from: '"男メイド" <yamada@danmaid.com>',
    to: 'yamada@danmaid.com'
}

// メール送信
var sender = new Rx.Subject()
// 地震発生ソース
var source = new EventSource('http://localhost:6914/follow')
source.onmessage = function (e) {
    console.log('source event received: ', e)
    let data = JSON.parse(e.data);
    let quake = {
        maxint: data.Report.Body[0].Intensity[0].Observation[0].MaxInt[0],
        hypocenter: data.Report.Body[0].Earthquake[0].Hypocenter[0].Area[0].Name[0],
        date: new Date(data.Report.Body[0].Earthquake[0].OriginTime[0]),
        magnitude: data.Report.Body[0].Earthquake[0]["jmx_eb:Magnitude"][0]._,
        coordinate: data.Report.Body[0].Earthquake[0].Hypocenter[0].Area[0]["jmx_eb:Coordinate"][0].$.description,
        pref: data.Report.Body[0].Intensity[0].Observation[0].Pref,
        key: data.Report.Head[0].EventID[0]
    }
    let message = {};
    message.original = data;
    message.key = quake.key;
    message.subject = '最大震度' + quake.maxint + 'の地震が' + quake.hypocenter + 'で発生したと思うんだ。';
    message.text = '発生日時: ' + quake.date.toLocaleString() + '\n';
    message.text += '震央: ' + quake.hypocenter + '\n';
    message.text += 'マグニチュード: ' + quake.magnitude + '\n';
    message.text += '震源: ' + quake.coordinate + '\n';
    message.text += '--- 各地の震度は ---\n';
    quake.pref.forEach(element => {
        message.text += element.Name[0] + ': ' + element.MaxInt[0]
    });
    sender.next(message);
}
source.onerror = function (e) {
    console.log('source error: ', e)
}
source.onopen = function (e) {
    console.log('source open: ', e)
}

// PINGチェックソース
var sourcePing = new EventSource('http://localhost:6916/follow')
sourcePing.onmessage = function (e) {
    console.log('source PING event received: ', e)
    let data = JSON.parse(e.data);
    let message = {};
    message.original = data;
    message.key = data.target.Report.Head[0].EventID[0];
    message.text = 'PING確認結果\n';
    data.result.forEach(host => {
        message.text += host.alive ? 'OK' : 'NG';
        message.text += ' ' + host.name + ' ' + host.hostname + '\n';
    });
    sender.next(message);
}
sourcePing.onerror = function (e) {
    console.log('source PING error: ', e)
}
sourcePing.onopen = function (e) {
    console.log('source PING open: ', e)
}
// header.subscribe(next => console.log('header: ', next));
// body.subscribe(next => console.log('body: ', next));

var header = {};

sender
    .groupBy(x => x.key)
    .subscribe(group => {
        group.take(1).subscribe(next => {
            console.log('first: ', next);
            let transport = mailer.createTransport(mailSettings);
            let mail = JSON.parse(JSON.stringify(mailOptions));
            mail.subject = next.subject;
            mail.text = next.text;
    
            transport.sendMail(mail, function (err, info) {
                if (err) {
                    return console.log(err.message);
                }
                console.log('Message sent: ', info, this);
                header[next.key] = {
                    inReplyTo: info.messageId,
                    references: [info.messageId],
                    subject: 'Re: ' + this.subject
                };

                transport.close();
            }.bind(next));
        });

        group.skip(1).subscribe(next => {
            console.log('sendmail: ', next);
            let transport = mailer.createTransport(mailSettings);
            let mail = JSON.parse(JSON.stringify(mailOptions));
            if (header[next.key]) {
                mail.subject = header[next.key].subject;
                mail.inReplyTo = header[next.key].inReplyTo;
                mail.references = header[next.key].references;
            }
            mail.text = next.text;
    
            transport.sendMail(mail, function (err, info) {
                if (err) {
                    return console.log(err.message);
                }
                console.log('Message sent: ', info, this);
                header[next.key].inReplyTo = info.messageId;
                header[next.key].references.push(info.messageId);
                transport.close();
            }.bind(next));
        });
    });


