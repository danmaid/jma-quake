const express = require('express')
const events = require('events')
const EventSource = require('eventsource')
const xml2js = require('xml2js')
const fetch = require('node-fetch')

const app = express()
const broker = new events.EventEmitter()

setInterval(function() {
    broker.emit('keepalive')
}, 30000)

setInterval(() => {
    fetch('http://www.data.jma.go.jp/developer/xml/feed/eqvol.xml')
    .then(x => {
        console.log(x)
        return x.text()
    })
    .then(xml => {
        console.log(xml)
        xml2js.parseString(xml, (err, result) => {
            // console.log(JSON.stringify(result))
            broker.emit('event', result)
        })
    })
}, 30000)

app.use(express.static(__dirname+ '/public'))

app.get('/follow', function (req,res) {
    console.log('connected')
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Access-Control-Allow-Origin': '*'
    })
    res.write('\n')

    var f = function(d) {
        res.write('data: ' + JSON.stringify(d) + '\n\n')
        res.flushHeaders()
    }
    broker.on('event', f)

    var k = function(d) {
        res.write(': keepalive\n\n')
        res.flushHeaders()
    }
    broker.on('keepalive', k)

    req.on('close', function() {
        broker.removeListener('event', f)
        broker.removeListener('keepalive', k)
        console.log('closed')
    })
})

app.listen(6911, function () {
   console.log('Aggregator start at port: 6911')
})
