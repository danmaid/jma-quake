const express = require('express')
const events = require('events')
const EventSource = require('eventsource')
const ping = require('ping')
const Rx = require('rxjs/Rx')

const app = express()
const broker = new events.EventEmitter()

const hosts = [
    { ip: '127.0.0.1', name: 'ローカル1号', hostname: 'localhost' },
    { ip: '192.168.111.111', name: 'ローカル2号', hostname: 'localhost' }
]

var destination = new Rx.Subject()
var source = new EventSource('http://localhost:6914/follow')
source.onmessage = function(e) {
    console.log('source event received: ', e)
    // JSONを展開してストリームに流す
    destination.next(JSON.parse(e.data))
}
source.onerror = function(e) {
    console.log('source error: ', e)
}
source.onopen = function(e) {
    console.log('source open: ', e)
}


var observerPing = function(data) {
    console.log('source event received: ', data)

    let callBacks = [];
    let h = JSON.parse(JSON.stringify(hosts));
    h.forEach(host => {
        let ret = ping.promise.probe(host.ip).then(function(res) {
            let result = this;
            result.alive = res.alive;
            console.log('then: ', res)
            return result;
        }.bind(host));
        callBacks.push(ret);
    });
    // 全部終わったら結果を emit する
    Promise.all(callBacks).then(function(res) {
        let result = {
            target: this,
            result: res
        }
        console.log('res: ', res)
        console.log('compose: ', JSON.stringify(res))
        broker.emit('event', result)
    }.bind(data));
};

destination.subscribe(observerPing);
destination.delay(60000).subscribe(observerPing);
destination.delay(180000).subscribe(observerPing);


setInterval(function() {
    broker.emit('keepalive')
}, 30000)

app.use(express.static(__dirname+ '/public'))

app.get('/status', function(req, res) {
    let status = {
        'readyState': source.readyState,
        'url': source.url,
        'clientCount': broker.listenerCount('event')
    }
    console.log(broker.listeners('event'))
    res.header('Content-Type', 'application/json; charset=utf-8')
    res.send(JSON.stringify(status))
})

app.get('/follow', function (req,res) {
    console.log('connected')
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Access-Control-Allow-Origin': '*'
    })
    res.write('\n')

    var f = function(d) {
        res.write('data: ' + JSON.stringify(d) + '\n\n')
        res.flushHeaders()
    }
    broker.on('event', f)

    var k = function(d) {
        res.write(': keepalive\n\n')
        res.flushHeaders()
    }
    broker.on('keepalive', k)

    req.on('close', function() {
        broker.removeListener('event', f)
        broker.removeListener('keepalive', k)
        console.log('closed')
    })
})

app.listen(6916, function () {
   console.log('Aggregator start at port: 6916')
})
