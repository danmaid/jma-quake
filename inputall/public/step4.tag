<step4>
    <div if="{ !data }">loading...</div>
    <table if="{ data }">
        <caption>
            <div>last fetched: { (new Date).toLocaleString() }</div>
        </caption>
        <thead>
            <tr>
                <th>発生日時</th>
                <th>震央地名</th>
                <th>マグニチュード</th>
                <th>最大震度</th>
                <th>震源位置</th>
            </tr>
        </thead>
        <tbody>
            <tr each="{ data }">
                <td>{ (new Date(Report.Body[0].Earthquake[0].OriginTime[0])).toLocaleString() }</td>
                <td>{ Report.Body[0].Earthquake[0].Hypocenter[0].Area[0].Name[0] }</td>
                <td>{ Report.Body[0].Earthquake[0]["jmx_eb:Magnitude"][0]._ }</td>
                <td>{ Report.Body[0].Intensity[0].Observation[0].MaxInt[0] }</td>
                <td>{ Report.Body[0].Earthquake[0].Hypocenter[0].Area[0]["jmx_eb:Coordinate"][0].$.description }</td>
            </tr>
        </tbody>
    </table>

    <script>
    this.data = new Array()
    this.es = this.opts.stream
    this.es.onmessage = function(e) {
        console.debug('received.')
        this.data.unshift(JSON.parse(e.data))
        console.debug(this.data)

        this.update()
    }.bind(this)
    </script>
</step4>
