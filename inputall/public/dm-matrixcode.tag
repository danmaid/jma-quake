<dm-matrixcode>
    <!-- Matrix code のように表示するアレ -->
    <canvas id="canvas"></canvas>

    <style>
        :scope {
            display: block;
        }
    </style>
    <script>
        // mount('dm-matrixcode', { stream: EventSource })
        // stream.subscribe(text) に対し、 text.toString() した結果を用いる
        this.drawing = new Array() // 描画対象の入れ物。ここに入ったものを書いていく

        this.draw = function() {
            this.canvasCtx.fillStyle = 'rgba(0,0,0,.1)'
            this.canvasCtx.fillRect(0, 0, this.width, this.height)
            this.drawing.map(function (data, index) {
                char_pos = data.char_pos < data.text.length ? data.char_pos : 0;
                char = data.text.charAt(char_pos);
                data.char_pos = char_pos + 1;
                this.canvasCtx.fillText(char, data.x_pos, data.y_pos);
                this.canvasCtx.fillStyle = data.color;
                (data.y_pos > (this.height + data.text.length * 10)) ? this.drawing.splice(index, 1) : data.y_pos += 10;
            }.bind(this))
        }.bind(this)

        this.on('mount', function() {
            // マウントされたらキャンバスサイズをセット
            this.canvas = this.root.querySelector('#canvas')
            this.canvasCtx = this.canvas.getContext('2d')
            this.width = this.canvas.width = this.root.clientWidth
            this.height = this.canvas.height = this.root.clientHeight

            if (!this.opts.stream instanceof EventSource) {
                console.error('stream is not EventSource!')
                return
            }

            this.opts.stream.onmessage = function(e) {
                console.log(JSON.parse(e.data))
                this.drawing.push({
                    'y_pos': 0,
                    'x_pos': Math.floor(Math.random() * ((this.width / 10) + 1)) * 10,
                    'char_pos': 0,
                    'text': e.data.toString().substr(0,100),
                    'color': '#0F0'
                });
            }.bind(this)

            // 描画開始
            setInterval(this.draw, 100)
        })
    </script>
</dm-matrixcode>