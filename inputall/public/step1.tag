<step1>
    <div if="{ !data }">loading...</div>
    <table if="{ data }">
        <caption>
            <div>title: { data.feed.title[0] }</div>
            <div>updated: { (new Date(data.feed.updated[0])).toLocaleString() }</div>
            <div>last fetched: { (new Date).toLocaleString() }</div>
        </caption>
        <thead>
            <tr>
                <th>title</th>
                <th>updated</th>
                <th>guid</th>
                <th>link</th>
                <th>content</th>
            </tr>
        </thead>
        <tbody>
            <tr each="{ data.feed.entry }">
                <td>{ title[0] }</td>
                <td>{ (new Date(updated[0])).toLocaleString() }</td>
                <td>{ id[0] }</td>
                <td>{ link[0].$.href }</td>
                <td><div class="container">{ content[0]._ }</div></td>
            </tr>
        </tbody>
    </table>

    <script>
    this.es = this.opts.stream
    this.es.onmessage = function(e) {
        console.debug('received.')
        this.data = JSON.parse(e.data)
        console.debug(this.data)

        this.update()
    }.bind(this)
    </script>
</step1>
