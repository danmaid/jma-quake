<ping>
    <div if="{ !data }">loading...</div>
    <table if="{ data }">
        <caption>
            <div>last fetched: { (new Date).toLocaleString() }</div>
        </caption>
        <thead>
            <tr>
                <th>イベントID</th>
                <th>チェック対象</th>
                <th>結果</th>
                <th>ホスト名</th>
                <th>IP</th>
            </tr>
        </thead>
        <tbody each="{ event in data }">
            <tr each="{ host in event.result }">
                <td>{ event.target.Report.Head[0].EventID[0] }</td>
                <td>{ host.name }</td>
                <td>{ host.alive }</td>
                <td>{ host.hostname }</td>
                <td>{ host.ip }</td>
            </tr>
        </tbody>
    </table>

    <script>
    this.data = new Array()
    this.es = this.opts.stream
    this.es.onmessage = function(e) {
        console.debug('received.', JSON.parse(e.data))
        this.data.unshift(JSON.parse(e.data))
        console.debug(this)

        this.update()
    }.bind(this)
    </script>
</ping>
