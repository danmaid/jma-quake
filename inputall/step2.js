var express = require('express')
var events = require('events')
var EventSource = require('eventsource')
var Rx = require('rxjs/Rx')

var app = express()
var broker = new events.EventEmitter()

setInterval(function() {
    broker.emit('keepalive')
}, 30000)

var destination = new Rx.Subject()
var source = new EventSource('http://localhost:6911/follow')
source.onmessage = function(e) {
    console.log('source event received: ', e)
    // JSONを展開してストリームに流す
    destination.next(JSON.parse(e.data))
}
source.onerror = function(e) {
    console.log('source error: ', e)
}
source.onopen = function(e) {
    console.log('source open: ', e)
}

destination
.flatMap(x => x.feed.entry) // ITEMリストを展開する
.filter(x => x.title == '震源・震度に関する情報')
// .distinct(x => x.guid) // GUIDで一意にする
.subscribe(next => {
    console.log('next: ', next)
    broker.emit('event', next)
})

app.use(express.static(__dirname+ '/public'))

app.get('/status', function(req, res) {
    let status = {
        'readyState': source.readyState,
        'url': source.url,
        'clientCount': broker.listenerCount('event')
    }
    console.log(broker.listeners('event'))
    res.header('Content-Type', 'application/json; charset=utf-8')
    res.send(JSON.stringify(status))
})

app.get('/follow', function (req,res) {
    console.log('accepts: ', req.accepts('text/event-stream'))
    console.log('connected')
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Access-Control-Allow-Origin': '*'
    })
    res.write('\n')

    var f = function(d) {
        res.write('data: ' + JSON.stringify(d) + '\n\n')
        res.flushHeaders()
    }
    broker.on('event', f)

    var k = function(d) {
        res.write(': keepalive\n\n')
        res.flushHeaders()
    }
    broker.on('keepalive', k)

    req.on('close', function() {
        broker.removeListener('event', f)
        broker.removeListener('keepalive', k)
        console.log('closed')
    })
})

app.listen(6912, function () {
   console.log('Aggregator start at port: 6912')
})
