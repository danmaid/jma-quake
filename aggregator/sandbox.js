var express = require('express')
var events = require('events')
var EventSource = require('eventsource')
var Rx = require('rxjs/Rx')
var sse = require('./sse')

var app = express()
var broker = sse.NewServer()

app.get('/', function (req,res) {
  res.sendFile(__dirname+ '/public/index.html')
})

setInterval(function() {
    broker.emit('testevent', new Date())
}, 3000)

app.get('/follow', broker)

app.listen(6912, function () {
   console.log('Aggregator start at port: 6912')
})
