var express = require('express')
var events = require('events')
var EventSource = require('eventsource')
var Rx = require('rxjs/Rx')

var app = express()
var broker = new events.EventEmitter()

setInterval(function() {
    broker.emit('keepalive')
}, 30000)

app.get('/', function (req,res) {
  res.sendFile(__dirname+ '/public/index.html')
})

var quakeSource = new Rx.Subject()
var source6911 = new EventSource('http://localhost:6911/follow')
source6911.onmessage = function(e) {
    console.log('event received: ', e)
    // JSONを展開してストリームに流す
    quakeSource.next(JSON.parse(e.data))
}
quakeSource
.flatMap(x => x.items) // ITEMリストを展開する
.filter(x => x.title == '震源・震度に関する情報')
.distinct(x => x.guid) // GUIDで一意にする
.subscribe(next => {
    console.log('next: ', next)
    broker.emit('quake', next)
})

app.get('/follow', function (req,res) {
    console.log('connected')
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    })
    res.write('\n')

    var f = function(d) {
        res.write('data: ' + JSON.stringify(d) + '\n\n')
        res.flushHeaders()
    }
    broker.on('quake', f)
    var k = function(d) {
        res.write(': keepalive\n\n')
        res.flushHeaders()
    }
    broker.on('keepalive', k)

    req.on('close', function() {
        broker.removeListener('quake', f)
        console.log('closed')
    })
})

app.listen(6912, function () {
   console.log('Aggregator start at port: 6912')
})
