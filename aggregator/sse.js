var events = require('events')

function (req,res) {
    console.log('connected')
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    })
    res.write('\n')

    var f = function(d) {
        res.write('data: ' + JSON.stringify(d) + '\n\n')
        res.flushHeaders()
    }
    broker.on('quake', f)
    var k = function(d) {
        res.write(': keepalive\n\n')
        res.flushHeaders()
    }
    broker.on('keepalive', k)

    req.on('close', function() {
        broker.removeListener('quake', f)
        console.log('closed')
    })
}

export var NewServer = function() {
    var broker = new events.EventEmitter()
    setInterval(function() {
        broker.emit('keepalive')
    }, 30000)
    
    return broker
}