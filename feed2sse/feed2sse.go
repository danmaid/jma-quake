package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/danmaid/live/core/server"
	"github.com/mmcdole/gofeed"

	_ "net/http/pprof"
)

func main() {
	broker := server.NewServer()
	go func() {
		for {
			fp := gofeed.NewParser()
			feed, _ := fp.ParseURL("http://www.data.jma.go.jp/developer/xml/feed/eqvol.xml")
			log.Println(feed)

			jsonBytes, err := json.Marshal(feed)
			if err != nil {
				log.Println("JSON Marshal error:", err)
				return
			}

			broker.Push <- jsonBytes

			// for _, item := range feed.Items {
			// 	fmt.Println(item.GUID)
			// 	fmt.Println(item.Link)

			// 	res, err := http.Get(item.Link)
			// 	if err != nil {
			// 		log.Println("fetch error: ", err)
			// 		continue
			// 	}
			// 	defer res.Body.Close()
			// 	json, err := xml2json.Convert(res.Body)
			// 	if err != nil {
			// 		fmt.Println("XML convert error:", err)
			// 		continue
			// 	}
			// 	// fmt.Println(json)
			// 	broker.Push <- json.Bytes()
			// }

			// res, _ := http.Get("http://www.data.jma.go.jp/developer/xml/data/65794721-e4f9-337d-b8ba-7f9632701759.xml")
			// defer res.Body.Close()
			// json, err := xml2json.Convert(res.Body)
			// if err != nil {
			// 	fmt.Println("XML convert error:", err)
			// 	return
			// }
			// fmt.Println(json)
			// broker.Push <- json.Bytes()
			time.Sleep(time.Second * 60)
		}
	}()
	http.Handle("/follow", broker)
	http.Handle("/", http.FileServer(http.Dir("public")))
	if err := http.ListenAndServe(":6911", nil); err != nil {
		log.Fatal("HTTP server error: ", err)
	}
}
